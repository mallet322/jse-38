import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

    public static final Logger logger = LogManager.getLogger(Application.class);
    private static final String JOKES_CATEGORIES = "https://api.chucknorris.io/jokes/categories";
    private static final String JOKES_RANDOM_CATEGORY = "https://api.chucknorris.io/jokes/random?category={category}";
    private static List<String> categories = new ArrayList<>();
    private static HttpURLConnection urlConnection;

    public static void main(String[] args) {
        readCategories();
        categories.forEach(Application::readJokes);
        urlConnection.disconnect();
    }

    private static void readCategories() {
        try {
            StringBuilder request = getRequest(new URL(JOKES_CATEGORIES));
            ObjectMapper objectMapper = new ObjectMapper();
            JavaType type = objectMapper.getTypeFactory().constructCollectionType(List.class, String.class);
            categories = objectMapper.readValue(request.toString(), type);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @SneakyThrows
    private static void readJokes(String category) {
        StringBuilder request = getRequest(new URL(JOKES_RANDOM_CATEGORY.replace("{category}", category)));
        String result = request.toString();
        String substr = "\"value\"";
        if (result.contains(substr)) {
            result = result.substring(result.indexOf(substr) + substr.length());
            logger.info(category.concat(" ").concat(result.replace("}null", "")));
        }
    }

    private static StringBuilder getRequest(URL url) {
        StringBuilder request = new StringBuilder();
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String input;
            do {
                input = reader.readLine();
                request.append(input);
            } while (input != null);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return request;
    }

}